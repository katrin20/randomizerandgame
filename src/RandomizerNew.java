import java.util.Scanner;

public class RandomizerNew {
    Scanner scan = new Scanner(System.in);
    String input;
    int randomQuantity;
    int[] rangeForRandom;
    boolean isNeedDuplicate;
    int max;
    int min;
    int size;
    int num;
    int[] random_num = new int[max];
    int count = 0;

    RandomizerNew(int randomQuantity, int[] rangeForRandom, boolean isNeedDuplicate) {
        this.randomQuantity = randomQuantity;
        this.rangeForRandom = rangeForRandom;
        this.isNeedDuplicate = isNeedDuplicate;
    }

    void start(int[] rangeForRandom) {

        min = rangeForRandom[0];
        max = rangeForRandom[1];
        System.out.println("min и max значение:\n" + min + " " + max);
        size = max - min;
        System.out.println("Чтобы продолжить введите любой символ, для завершения введите exit");
        input = scan.next();
        isNeedDuplicateOrNot(isNeedDuplicate);
    }

    void isNeedDuplicateOrNot(boolean isNeedDuplicate) {
        if (isNeedDuplicate) {
            generateDuplicatNumber();
        }else generateNotDuplicatNumber();
    }

    void generateDuplicatNumber() {
        while (!input.equals("exit")) {
            if (count < size) {
                num = min + (int) (Math.random() * size);
                System.out.println(num);
                count++;
                input = scan.next();
            } else {
                System.out.println("В заданном диапазоне больше нет значений!");
                break;
            }
        }
        System.out.println("Bye!");
    }
    void generateNotDuplicatNumber() {
        do {
            if (input.equals("exit")) {
                System.out.println("Bye!");
                break;
            } else {
                num = min + (int) (Math.random() * size);
                if (random_num[num - 1] != num) {
                    random_num[num - 1] = num;
                    count++;
                    System.out.println(random_num[num - 1]);
                    input = scan.next();
                }
            }
        }
        while (count < size);
        System.out.println("В заданном диапазоне больше нет значений!\nBye!");
    }
}
