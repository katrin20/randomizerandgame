import java.io.IOException;

public class Client {
    public static void main(String[] args) throws IOException {
        GameFileReader gameFileReader = new GameFileReader("./src/properties.txt");
        gameFileReader.parse();
        RandomizerNew randomizerNew = new RandomizerNew(gameFileReader.getAttemptQuantity(), gameFileReader.getRandomizerRange(), gameFileReader.getIsNeedRepeat());
        randomizerNew.start(gameFileReader.getRandomizerRange());
        GuessNumber guessNumber= new GuessNumber(gameFileReader.getAttemptQuantity(),gameFileReader.getGuessNumberRange());
        guessNumber.showMessage();
    }
}
