import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class GameFileReader {
    String pathToFile;
    private int attemptQuantity;
    private int[] guessNumberRange;
    private boolean isNeedRepeat;
    private int[] randomizerRange;

    GameFileReader(String pathToFile) {

        this.pathToFile = pathToFile;

    }
    void parse() throws IOException {

        File file = new File(pathToFile);
        Properties properties = new Properties();
        properties.load(new FileReader(file));

        setData(properties);

    }

    private void setData(Properties properties) {
        setAttemptQuantity(Integer.parseInt(properties.getProperty("attemptQuantity")));


        String[] numberRangeString = (properties.getProperty("guessNumberRange")).split(",");
        int[] guessNumberRange = new int[numberRangeString.length];
        for (int i=0;i<guessNumberRange.length;i++) {
            guessNumberRange[i] = Integer.parseInt(numberRangeString[i]);
        }
        setGuessNumberRange(guessNumberRange);


        String needRepeat = properties.getProperty("isNeedRepeat");
        if (needRepeat.equals("true")) {
            isNeedRepeat = true;
        } else {
            isNeedRepeat = false;
        }
        setIsNeedRepeat(isNeedRepeat);


        String[] randomizerRangeString = (properties.getProperty("randomizerRange")).split(",");
        int[] randomizerRange = new int[randomizerRangeString.length];
        for (int i=0;i<randomizerRange.length;i++) {
            randomizerRange[i] = Integer.parseInt(randomizerRangeString[i]);
        }

        setRandomizerRange(randomizerRange);

    }

    public int getAttemptQuantity() {
        return attemptQuantity;
    }
    public void setAttemptQuantity(int attemptQuantity) {
        this.attemptQuantity = attemptQuantity;
    }

    public int[] getGuessNumberRange() {

        return guessNumberRange;
    }
    public void setGuessNumberRange(int [] guessNumberRange){
        this.guessNumberRange = guessNumberRange;
    }

    public boolean getIsNeedRepeat() {

        return isNeedRepeat;
    }

    public  void setIsNeedRepeat(Boolean isNeedRepeat){
        this.isNeedRepeat=isNeedRepeat;
    }

    public int[] getRandomizerRange() {

        return randomizerRange;
    }
    public void setRandomizerRange(int [] randomizerRange){
        this.randomizerRange=randomizerRange;
    }
}
