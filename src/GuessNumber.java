import java.util.Scanner;

public class GuessNumber {
    Scanner scan = new Scanner(System.in);
    private int attempt;
    private int userNum;
    private int myNum;
    private int count;
    private int nextDifference;
    private int [] numberRange;
    private int minNum;
    private int maxNum;
    private int size;
    private int difference;
    GuessNumber (int attempt, int[] numberRange){
        this.attempt=attempt;
        this.numberRange=numberRange;
    }

    void showMessage() {
        minNum=numberRange[0];
        maxNum=numberRange[1];
        size=maxNum-minNum;
        System.out.println("Привет. я загадал число от "+ minNum+" до "+maxNum+ ". У тебя есть "+attempt +" попыток отгадать его\n");
        System.out.println("Твоя первая попытка:");
        userNum = scan.nextInt();
        randomNumberForGame();
    }
    void randomNumberForGame() {
        myNum = minNum + (int) (Math.random() * size);
        getOdds(myNum);
    }

    void getOdds(int myNum) {
        count=attempt-1;
        do {
            difference = myNum - userNum;
            if (difference == 0) {
                System.out.println("Ты угадал");
                break;
            }
            if (difference != 0) {
                System.out.println("Осталось " + count + " попытки");
                userNum = scan.nextInt();
                nextDifference = myNum - userNum;
                if (Math.abs(difference) >= Math.abs(nextDifference)) {
                    System.out.println("Теплее");
                } else System.out.println("Холоднее");
                count--;
            }
        }
        while (count > 0);
    }
}
